const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseControllers = require("../controllers/courseControllers");

console.log(courseControllers);

// Route for creating course
router.post("/", auth.verify, courseControllers.addCourse);

// Route for viewing all courses (admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Route for viewing all active courses (all users)
router.get("/", courseControllers.getAllActive);

// Route for viewing a specific course
router.get("/:courseId", courseControllers.getCourse);

// Route for updating a course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// Route for archiving a course
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);

module.exports = router;