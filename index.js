const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// allows our backend application to be available with our frontend application
const cors = require("cors");

const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.qe9fcvk.mongodb.net/batch203_bookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
// Allow all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/users", userRoutes);

app.use("/courses", courseRoutes);

// This syntax will allow flexibility when using the application locally or as a hosted app.
const port = process.env.PORT || 4000

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});
