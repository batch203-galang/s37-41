/*
user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					courseId - the unique identifier for the course,
					courseName - optional,
					status,
					isPaid,
					dateEnrolled
				}
			]

		}

*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createOn: {
		type: Date,
		default: new Date()
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required"]
			},
			courseName: {
				type: String,
				required: [true, "Course name is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			isPaid: {
				type: Boolean,
				default: true
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);