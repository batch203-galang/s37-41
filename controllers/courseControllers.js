const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

/*
s39 Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
4. Update your remote link and push to git with the commit message of Add activity code - S39.
5. Add the link in Boodle.
*/

module.exports.addCourse = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let newCourse = new Course
        ({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
        })

    if (userData.isAdmin == true){

        newCourse.save()

        .then(course => {
            console.log(course);
            res.send(true);
        })

        // Course not saved
        .catch(error => {
            console.log(error);
            res.send(false);
        })
    } 
    else {
        res.send("You're not an admin!")
    }
};


// Retrieve all courses
/*
	Steps:
	1. Retreive all the courses (active/inactive) from the database.
	2. Verify the role of the current user.

*/

module.exports.getAllCourses = (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		return Course.find({})
		.then(result => res.status(200).send(result))
	}
	else {
        res.status(401).send("You're not an admin!")
    }

}

// Retrieve all active courses
/*
	Step:
	1. Retrieve all the courses from the database with the property "isActive" to true.
*/


module.exports.getAllActive = (req, res) => {
	return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific course
/*
	Step:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (req, res) => {
	console.log(req.params.courseId);

	return Course.findById(req.params.courseId).then(result => res.send(result));
}

// Update the course
/*
	Step:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/


module.exports.updateCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}
	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied, {new: true})

		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new: true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else {
		// return res.send(false);
        res.status(401).send("You're not an admin!")
    }
}


// Archive a course
	// Soft delete happens when a course status "isActive" is set to false

module.exports.archiveCourse = (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin) {
		return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField, {new:true})
		.then(result => {
			console.log(result)
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else {
		// return res.send(false);
        res.status(401).send("You're not an admin!");
	}
}