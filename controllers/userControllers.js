const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if the email exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req,res) => {
	return User.find({email: req.body.email})
	.then(result =>{

		// The result of the find() method returns an array of objects.
			// we can use array.length method for checking the current result length.
		console.log(result);
		// the user already exists
		if(result.length > 0){
			return res.send("User already exists");
		} 
		// There are no duplicates found
		else {
			return res.send("No duplicate found!");
		}

	})
	.catch(error => res.send(error));
}

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt);
		// salt - salt rounds that the bcrypt algorithm will run to encrypt the password.
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})
	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);
		res.send(true);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	})
}

// User authentication (login)
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		// User does not exist
		if(result == null) {
			// return res.send(false);
			return res.send({message: "No user found!"});
		} 
		// User exists
		else { 
			// Syntax: compareSync(data, encrypted)
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			// If the password match/result of the above code is true.
			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else {
				return res.send({message: "Incorrect password!"});
			}
		}
	})
}

// S38 Activity

/*

1. Create a "/details/:id" route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
    - Find the document in the database using the user's ID
    - Reassign the password of the returned document to an empty string ("") / ("*****")
    - Return the result back to the postman
3. Process a GET request at the /details/:id route using postman to retrieve the details of the user.
4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
5. Add the link in Boodle.

*/

// module.exports.getProfileController = (req,res) => {
// 	User.findById(req.params.userId)
// 	.then(result => {
// 		if(result == null){
// 			return res.send({message: "No user found!"})
// 		}
// 		else {
// 			result.password = "******";
// 			res.send(result);
// 		}
// 	})
// 	.catch(error => res.send(error))
// };

module.exports.getProfileController = (req,res) => {
	
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id)
	.then(result => {
		result.password = "****";
		res.send(result);
	})
};

// Enroll a course

/*
Steps:
	// Users
	1. Find the document in the database using the user's ID (token)
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
	//Courses
	1. Find the document in the database using the course ID provided in the request body.
	2. Add the user ID to the course enrollees array.
	3. Update the document in the MongoDB Atlas Database

*/

// Async await will be used in enrolling the user because we will need to update 2 seperate documents when enrolling a user.
module.exports.enroll = async (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let courseName = await Course.findById(req.body.courseId).then(result => result.name)

	let data = {
		// User ID and email will be retrieved from the 
		userId: userData.id,
		email: userData.email,
		// course ID will be retrieved from the req.body
		courseId: req.body.courseId,
		// courseName value is retreived using the findById method
		courseName: courseName
	}

	console.log(data);

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({
			courseId: data.courseId,
			courseName: data.courseName
		});

		// Save the updated user information in the database
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated)

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({
			userId: data.userId,
			email: data.email
		})

		// Minus the slots available by 1
		course.slots -= 1;

		return course.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isCourseUpdated);

	// Condition will check if the both "user" and "course" document have been updated.
	
	(isUserUpdated && isCourseUpdated) ? res.send(true) : res.send(false)
}